<?php

require_once __DIR__ . '/vendor/autoload.php';

use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Component\HttpFoundation\Request;
use ZL\ChatToolSDK\ChatBot\ChatBot;
use ZL\ChatToolSDK\ChatBot\HandlerInterface;
use ZL\ChatToolSDK\ChatBot\Model\IncomingRequest;
use ZL\ChatToolSDK\ChatBot\Model\Response;
use ZL\ChatToolSDK\ChatBot\Model\ResponseMessage;

// We need to regist annotations for model serialization
AnnotationRegistry::registerAutoloadNamespace(
    'JMS\Serializer\Annotation',
    __DIR__ . "/vendor/jms/serializer/src");

$bot = new ChatBot();

/**
 * Set default handler.
 * This handler will be called if no handler for given intend is found
 *
 * In this example anonymous classes are used. But every class with implemented HandlerInterface can be used.
 */
$bot->setDefaultHandler(new class() implements HandlerInterface
{
    public function handle(IncomingRequest $incomingRequest, Response $response)
    {
        // We response only if we have received exactly 3 messages from client
        if ($incomingRequest->getSession()->getReceivedMessages() == 3) {
            $response->addMessage(new ResponseMessage('To write more message please download our app at: example.org!'));

        // If we have received more than 3 messages, then we will ignore all other messages
        } else if ($incomingRequest->getSession()->getReceivedMessages() > 3) {

            // Set "Response::stopProcessing" to true, if the message should not be redirected to an agent.
            $response->setStopProcessing(true);
        }

        // If no messages is set and Response::stopProcessing is false,
        // the message will be redirected to an agent
        return;
    }
});

/**
 * Set handler for intend "TIME".
 */
$bot->addHandler('TIME', new class() implements HandlerInterface
{
    public function handle(IncomingRequest $incomingRequest, Response $response)
    {
        // Possible keywords are: (First, FirstSecond and Second) word in a message
        if (in_array('date', $incomingRequest->getMessage()->getPossibleKeywords())) {
            $response->addMessage(new ResponseMessage(sprintf('The current date is %s', date('m.d.Y'))));

        } else {
            $response->addMessage(new ResponseMessage(sprintf('The current time is %s', date('H:i'))));
        }
    }
});

// Handle the incoming request
$response = $bot->handleRequest(
    Request::createFromGlobals()
);

// Send the response to the client
$response->send();
