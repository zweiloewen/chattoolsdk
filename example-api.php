<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

require_once __DIR__ . '/vendor/autoload.php';

// We need to regist annotations for model serialization
AnnotationRegistry::registerAutoloadNamespace(
    'JMS\Serializer\Annotation',
    __DIR__ . "/vendor/jms/serializer/src");

// Create api client
$client = new \ZL\ChatToolSDK\Messaging\ApiClient([
    'token' => 'test',
    'api_endpoint' => 'http://chattool.dev/app_dev.php'
]);

var_dump($client->sendMultipleMessages([
    new \ZL\ChatToolSDK\Messaging\Model\Message('eee', 'rrr', 'efef'),
    new \ZL\ChatToolSDK\Messaging\Model\Message('eee', 'rrr', 'efef')
]));

//var_dump($client->sendMessage(new \ZL\ChatToolSDK\Messaging\Model\Message('eee', 'rrr', 'efef')));