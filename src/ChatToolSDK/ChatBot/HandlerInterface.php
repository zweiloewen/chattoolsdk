<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 12:23
 */

namespace ZL\ChatToolSDK\ChatBot;

use ZL\ChatToolSDK\ChatBot\Model\IncomingRequest;
use ZL\ChatToolSDK\ChatBot\Model\Response;

interface HandlerInterface
{
    /**
     * @param IncomingRequest $incomingRequest
     * @param Response $response
     * @return void
     */
    public function handle(IncomingRequest $incomingRequest, Response $response);
}
