<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 12:33
 */

namespace ZL\ChatToolSDK\ChatBot\Model;

use JMS\Serializer\Annotation as JMS;

class Message
{
    /**
     * @var string
     * @JMS\Type("string")
     */
    private $messageGuid;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $content;

    /**
     * @var array
     * @JMS\Type("array<string>")
     */
    private $possibleKeywords;

    /**
     * @var \DateTime
     * @JMS\Type("DateTime<'Y-m-d H:i:s', 'Europe/Berlin'>")
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getMessageGuid()
    {
        return $this->messageGuid;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public function getPossibleKeywords()
    {
        return $this->possibleKeywords;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
