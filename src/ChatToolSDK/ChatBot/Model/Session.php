<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 12:29
 */

namespace ZL\ChatToolSDK\ChatBot\Model;

use JMS\Serializer\Annotation as JMS;

class Session
{
    /**
     * @var string
     * @JMS\Type("string")
     */
    private $sessionId;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $transactionGuid;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $service;

    /**
     * @var string
     * @JMS\Type("integer")
     */
    private $receivedMessages;

    /**
     * @var string
     * @JMS\Type("integer")
     */
    private $sentMessages;

    /**
     * @var \DateTime
     * @JMS\Type("DateTime<'Y-m-d H:i:s', 'Europe/Berlin'>")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @JMS\Type("DateTime<'Y-m-d H:i:s', 'Europe/Berlin'>")
     */
    private $updatedAt;

    /**
     * @var array
     * @JMS\Type("array<string,string>")
     */
    private $meta = [];

    /**
     * @return mixed
     */
    public function getSessionId()
    {
        return (string) $this->sessionId;
    }

    /**
     * @return mixed
     */
    public function getTransactionGuid()
    {
        return (string) $this->transactionGuid;
    }

    /**
     * @return int
     */
    public function getReceivedMessages()
    {
        return (int) $this->receivedMessages;
    }

    /**
     * @return int
     */
    public function getSentMessages()
    {
        return (int) $this->sentMessages;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }
}
