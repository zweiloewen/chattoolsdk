<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 13:28
 */

namespace ZL\ChatToolSDK\ChatBot\Model;

use JMS\Serializer\Annotation as JMS;

class Response
{
    /**
     * @var Bot
     * @JMS\Type("ZL\ChatToolSDK\ChatBot\Model\Bot")
     */
    private $bot;

    /**
     * @var Session
     * @JMS\Type("ZL\ChatToolSDK\ChatBot\Model\Session")
     */
    private $session;

    /**
     * @var ResponseMessage[]
     * @JMS\Type("array<ZL\ChatToolSDK\ChatBot\Model\ResponseMessage>")
     */
    private $messages = [];

    /**
     * @var bool
     * @JMS\Type("boolean")
     */
    private $stopProcessing = false;

    /**
     * Response constructor.
     * @param Bot $bot
     * @param Session $session
     * @param bool $stopProcessing
     */
    public function __construct(Bot $bot, Session $session, $stopProcessing = false)
    {
        $this->session = $session;
        $this->bot = $bot;
        $this->stopProcessing = $stopProcessing;
    }

    /**
     * You can add response messages.
     * If no message exists, the session will be redirected to an agent.
     *
     * @param ResponseMessage $message
     */
    public function addMessage(ResponseMessage $message)
    {
        $this->messages[] = $message;
    }

    /**
     * @return ResponseMessage[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return Bot
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }


    /**
     * @return bool
     */
    public function isStopProcessing()
    {
        return $this->stopProcessing;
    }

    /**
     * @param bool $stopProcessing
     */
    public function setStopProcessing($stopProcessing)
    {
        $this->stopProcessing = $stopProcessing;
    }
}
