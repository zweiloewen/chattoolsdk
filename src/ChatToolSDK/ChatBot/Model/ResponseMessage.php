<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 13:29
 */

namespace ZL\ChatToolSDK\ChatBot\Model;

use JMS\Serializer\Annotation as JMS;

class ResponseMessage
{
    CONST TYPE_TEXT = 'text';
    CONST TYPE_MEDIA = 'media';

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $type = 'text';

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $content;

    /**
     * ResponseMessage constructor.
     * @param string $type
     * @param string $content
     */
    public function __construct($content, $type = self::TYPE_TEXT)
    {
        $this->content = $content;
        $this->setType($type);
    }


    /**
     * @return string
     */
    public function getType()
    {
        return (string) $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        if (!in_array($type, [self::TYPE_MEDIA, self::TYPE_TEXT])) {
            throw new \InvalidArgumentException('Only type media or text is allowed!');
        }

        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return (string) $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}
