<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 12:34
 */

namespace ZL\ChatToolSDK\ChatBot\Model;

use JMS\Serializer\Annotation as JMS;

class IncomingRequest
{
    /**
     * @var Bot
     * @JMS\Type("ZL\ChatToolSDK\ChatBot\Model\Bot")
     */
    private $bot;

    /**
     * @var Session
     * @JMS\Type("ZL\ChatToolSDK\ChatBot\Model\Session")
     */
    private $session;

    /**
     * @var Message
     * @JMS\Type("ZL\ChatToolSDK\ChatBot\Model\Message")
     */
    private $message;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $intend;

    /**
     * @return Bot
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getIntend()
    {
        return mb_strtoupper($this->intend, 'UTF-8');
    }
}
