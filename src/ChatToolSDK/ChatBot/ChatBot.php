<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 12:22
 */

namespace ZL\ChatToolSDK\ChatBot;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use JsonSchema\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ZL\ChatToolSDK\ChatBot\Model\IncomingRequest;
use ZL\ChatToolSDK\ChatBot\Model\Response as ResponseModel;

class ChatBot
{
    /** @var HandlerInterface|null */
    protected $defaultHandler;

    /** @var HandlerInterface[] */
    protected $handlers = [];

    /** @var SerializerInterface */
    protected $serializer;

    /**
     * ChatBot constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer = null)
    {
        $this->serializer = $serializer ?: SerializerBuilder::create()->build();
    }

    /**
     * Handle an incoming chat tool request
     *
     * @param Request $request
     * @return Response
     */
    public function handleRequest(Request $request)
    {
        // Validate incoming json
        $data = @json_decode($request->getContent());

        $validator = new Validator();
        $validator->coerce($data, (object)['$ref' => 'file://' . realpath(__DIR__ . '/request.schema.json')]);

        // If json schema was invalid, create a error response
        if (!$validator->isValid()) {

            $errorMsg = [
                'error' => 'Invalid JSON Schema',
                'properties' => [],
            ];
            $onlyExtra = true;

            foreach ($validator->getErrors() as $error) {
                if ('session.meta' !== $error['property']) {
                    $onlyExtra = false;
                }

                $errorMsg['properties'][] = ['property' => $error['property'], 'message' => $error['message']];
            }

            if (!$onlyExtra) {
                $response = new Response($this->serializer->serialize($errorMsg, 'json'), Response::HTTP_BAD_REQUEST);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        }

        /** @var IncomingRequest $incomingRequest */
        $incomingRequest = $this->serializer
            ->deserialize(
                $request->getContent(),
                'ZL\ChatToolSDK\ChatBot\Model\IncomingRequest',
                'json'
            );

        $responseModel = new ResponseModel($incomingRequest->getBot(), $incomingRequest->getSession());

        $this
            ->getHandler($incomingRequest)
            ->handle($incomingRequest, $responseModel)
        ;

        $response = new Response($this->serializer->serialize($responseModel, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Add handler for a intend
     *
     * @param $intend
     * @param HandlerInterface $handler
     */
    public function addHandler($intend, HandlerInterface $handler)
    {
        $this->handlers[mb_strtoupper($intend, 'UTF-8')] = $handler;
    }

    /**
     * Add default handler
     *
     * @param HandlerInterface $handler
     */
    public function setDefaultHandler(HandlerInterface $handler)
    {
        $this->defaultHandler = $handler;
    }

    /**
     * @param IncomingRequest $request
     * @return HandlerInterface
     */
    protected function getHandler(IncomingRequest $request)
    {
        // No handler exists
        if (empty($this->handlers) && null === $this->defaultHandler) {
            throw new \LogicException('Cannot handle request without handler!');
        }

        // Only default handler exists
        if (empty($this->handlers) && null !== $this->defaultHandler) {
            return $this->defaultHandler;
        }

        // Handler for indent exists
        if (array_key_exists($request->getIntend(), $this->handlers)) {
            return $this->handlers[$request->getIntend()];
        }

        // No handler found, return default handler
        return $this->defaultHandler;
    }
}
