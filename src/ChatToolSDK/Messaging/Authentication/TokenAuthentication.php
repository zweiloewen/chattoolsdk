<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 02.05.2017
 * Time: 17:44
 */

namespace ZL\ChatToolSDK\Messaging\Authentication;

use Http\Message\Authentication;
use Psr\Http\Message\RequestInterface;

class TokenAuthentication implements Authentication
{
    /** @var string */
    private $token;

    /**
     * TokenAuthentication constructor.
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(RequestInterface $request)
    {
        return $request->withHeader('X-Token', $this->token);
    }
}
