<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 03.05.2017
 * Time: 13:06
 */

namespace ZL\ChatToolSDK\Messaging;


use ZL\ChatToolSDK\Messaging\Model\ReceiveMessageRequest;

interface RequestHandlerInterface
{
    /**
     * Handle an incoming message request
     * @param ReceiveMessageRequest $request
     *
     * @return void
     */
    public function handle(ReceiveMessageRequest $request);
}
