<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 03.05.2017
 * Time: 13:08
 */

namespace ZL\ChatToolSDK\Messaging\Model;

use JMS\Serializer\Annotation as JMS;

class ReceiveMessageRequest
{
    CONST TYPE_TEXT = 'text';
    CONST TYPE_MEDIA = 'media';

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $type;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("attachmentUrl")
     */
    private $attachmentUrl;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $message;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("transactionGuid")
     */
    private $transactionGuid;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("sessionId")
     */
    private $sessionId;

    /**
     * @var string|null
     * @JMS\Type("string")
     */
    private $agent;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $keyword;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function isMedia()
    {
        return $this->type === 'media';
    }

    public function isText()
    {
        return $this->type === 'text';
    }

    /**
     * @return string|null
     */
    public function getAttachmentUrl()
    {
        return $this->attachmentUrl;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return (string) $this->message;
    }

    /**
     * @return string
     */
    public function getTransactionGuid()
    {
        return (string) $this->transactionGuid;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return (string) $this->sessionId;
    }

    /**
     * @return null|string
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return (string) $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = (string) $keyword;
    }
}
