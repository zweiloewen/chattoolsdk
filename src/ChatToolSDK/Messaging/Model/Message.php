<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 02.05.2017
 * Time: 17:57
 */

namespace ZL\ChatToolSDK\Messaging\Model;


class Message
{
    CONST STYLE_NORMAL = 'normal';
    CONST STYLE_HIGHLIGHT = 'highlight';
    CONST STYLE_ATTENTION = 'attention';
    CONST STYLE_NOTIFICATION = 'notification';
    CONST STYLE_TRIGGER = 'trigger';
    CONST STYLE_KEYWORD = 'keyword';

    /**
     * @var string
     */
    private $transactionGuid;

    /**
     * @var string
     */
    private $messageGuid;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string[]
     */
    private $attachments;

    /**
     * @var string
     */
    private $keyword;

    /**
     * @var bool
     */
    private $deliver;

    /**
     * @var string
     */
    private $style = self::STYLE_NORMAL;

    /**
     * Message constructor.
     * @param string $transactionGuid
     * @param string $messageGuid
     * @param string $message
     */
    public function __construct($transactionGuid, $messageGuid, $message)
    {
        $this->transactionGuid = (string) $transactionGuid;
        $this->messageGuid = (string) $messageGuid;
        $this->message = (string) $message;
        $this->deliver = true;
    }

    /**
     * @return string
     */
    public function getTransactionGuid()
    {
        return $this->transactionGuid;
    }

    /**
     * @param string $transactionGuid
     */
    public function setTransactionGuid($transactionGuid)
    {
        $this->transactionGuid = (string) $transactionGuid;
    }

    /**
     * @return string
     */
    public function getMessageGuid()
    {
        return $this->messageGuid;
    }

    /**
     * @param string $messageGuid
     */
    public function setMessageGuid($messageGuid)
    {
        $this->messageGuid = (string) $messageGuid;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = (string) $message;
    }

    /**
     * @return \string[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param \string[] $attachments
     */
    public function setAttachments(array $attachments)
    {
        $this->attachments = $attachments;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return (string) $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return bool
     */
    public function isDeliver()
    {
        return $this->deliver;
    }

    /**
     * @param bool $deliver
     */
    public function setDeliver($deliver)
    {
        $this->deliver = boolval($deliver);
    }

    /**
     * @return string
     */
    public function getStyle()
    {
        return  $this->style;
    }

    /**
     * @param string $style
     */
    public function setStyle($style)
    {
        $this->style = (string) $style;
    }
}
