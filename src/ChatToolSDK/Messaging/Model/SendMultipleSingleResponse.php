<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 03.05.2017
 * Time: 12:55
 */

namespace ZL\ChatToolSDK\Messaging\Model;

use JMS\Serializer\Annotation as JMS;

class SendMultipleSingleResponse
{
    /**
     * @JMS\Type("integer")
     * @var int
     */
    private $code;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $status;

    /**
     * @var array
     * @JMS\Type("array")
     */
    private $headers;

    /**
     * @var array
     * @JMS\Type("array")
     */
    private $data;

    /**
     * @return int
     */
    public function getCode()
    {
        return (int) $this->code;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return (string) $this->status;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return (array) $this->headers;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return (array) $this->data;
    }
}
