<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 02.05.2017
 * Time: 22:55
 */

namespace ZL\ChatToolSDK\Messaging\Model;

use JMS\Serializer\Annotation as JMS;

class SendResponseData
{
    /**
     * @var string
     * @JMS\Type("string")
     */
    private $sessionId;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $transactionGuid;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $messageGuid;

    /**
     * @var bool
     * @JMS\Type("boolean")
     */
    private $needAdminCheck;

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @return string
     */
    public function getTransactionGuid()
    {
        return $this->transactionGuid;
    }

    /**
     * @return string
     */
    public function getMessageGuid()
    {
        return $this->messageGuid;
    }

    /**
     * @return bool
     */
    public function isNeedAdminCheck()
    {
        return $this->needAdminCheck;
    }
}
