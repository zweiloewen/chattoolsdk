<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 02.05.2017
 * Time: 22:54
 */

namespace ZL\ChatToolSDK\Messaging\Model;

use JMS\Serializer\Annotation as JMS;

class SendResponse
{
    /**
     * @JMS\Type("integer")
     * @var int
     */
    private $code;

    /**
     * @var string
     * @JMS\Type("string")
     */
    private $status;

    /**
     * @var SendResponseData
     * @JMS\Type("ZL\ChatToolSDK\Messaging\Model\SendResponseData")
     */
    private $data;

    /**
     * @return int
     */
    public function getCode()
    {
        return intval($this->code);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return (string) $this->status;
    }

    /**
     * @return SendResponseData
     */
    public function getData()
    {
        return $this->data;
    }
}
