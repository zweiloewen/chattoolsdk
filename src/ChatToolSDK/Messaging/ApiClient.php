<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 29.04.2017
 * Time: 15:46
 */

namespace ZL\ChatToolSDK\Messaging;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\ErrorPlugin;
use Http\Client\Common\PluginClient;
use Http\Client\HttpClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\UriFactoryDiscovery;
use Http\Message\RequestFactory;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ZL\ChatToolSDK\Messaging\Authentication\TokenAuthentication;
use ZL\ChatToolSDK\Messaging\Model\Message;
use ZL\ChatToolSDK\Messaging\Model\SendMultipleResponse;
use ZL\ChatToolSDK\Messaging\Model\SendResponse;

class ApiClient
{
    /** @var RequestFactory */
    protected $requestFactory;

    /** @var HttpClient */
    private $httpClient;

    /** @var \JMS\Serializer\Serializer */
    private $serializer;

    /** @var array */
    private $options;

    /**
     * ApiClient constructor.
     * @param array $options
     * @param RequestFactory $requestFactory
     * @param HttpClient $httpClient
     * @param SerializerInterface|null $serializer
     */
    public function __construct(array $options, RequestFactory $requestFactory= null, HttpClient $httpClient = null, SerializerInterface $serializer = null)
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);

        $this->serializer = $serializer ?: SerializerBuilder::create()->build();
        $this->requestFactory = $requestFactory ?: MessageFactoryDiscovery::find();

        $authPlugin = new AuthenticationPlugin(new TokenAuthentication($this->options['token']));
        $basePathPlugin = new BaseUriPlugin(UriFactoryDiscovery::find()->createUri($this->options['api_endpoint']));
        $errorPlugin = new ErrorPlugin();

        $this->httpClient = new HttpMethodsClient(
            new PluginClient($httpClient ?: HttpClientDiscovery::find(), [$authPlugin, $basePathPlugin, $errorPlugin]),
            $this->requestFactory
        );
    }

    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['token', 'api_endpoint']);
    }

    /**
     * Send a new message to the ChatTool Server.
     *
     * @param Message $message
     * @return SendResponse
     */
    public function sendMessage(Message $message)
    {
        $response = $this->httpClient->post(
            '/api/v1/thirdparty/receive.json',
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query([
                'transactionGuid' => $message->getTransactionGuid(),
                'messageGuid' => $message->getMessageGuid(),
                'message' => $message->getMessage(),
                'keyword' => $message->getKeyword(),
                'deliver' => intval($message->isDeliver()),
            ])
        );

        return $this->serializer
            ->deserialize(
                $response->getBody()->getContents(),
                'ZL\ChatToolSDK\Messaging\Model\SendResponse',
                'json'
            );
    }

    /**
     * Send multiple new messages to the ChatTool Server
     *
     * @param Message[] $messages
     * @return SendMultipleResponse
     */
    public function sendMultipleMessages(array $messages)
    {
        $out = ['messages' => []];
        foreach ($messages as $message) {
            $out['messages'][] = [
                'transactionGuid' => $message->getTransactionGuid(),
                'messageGuid' => $message->getMessageGuid(),
                'message' => $message->getMessage(),
                'keyword' => $message->getKeyword(),
                'deliver' => intval($message->isDeliver()),
            ];
        }

        $response = $this->httpClient->post(
            '/api/v1/thirdparty/receive_multiple',
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($out)
        );

        return $this->serializer
            ->deserialize(
                $response->getBody()->getContents(),
                'ZL\ChatToolSDK\Messaging\Model\SendMultipleResponse',
                'json'
            );
    }
}
