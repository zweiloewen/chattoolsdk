<?php
/**
 * User: Jens Averkamp <jens.averkamp@zweiloewen.com>
 * Date: 03.05.2017
 * Time: 13:10
 */

namespace ZL\ChatToolSDK\Messaging;


use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use ZL\ChatToolSDK\Messaging\Model\ReceiveMessageRequest;

class IncomingMessageHandler
{
    /** @var RequestHandlerInterface */
    protected $handler;

    /** @var SerializerInterface */
    protected $serializer;

    /**
     * IncomingMessageHandler constructor.
     * @param RequestHandlerInterface $handler
     * @param SerializerInterface|null $serializer
     */
    public function __construct(RequestHandlerInterface $handler = null, SerializerInterface $serializer = null)
    {
        $this->handler = $handler;
        $this->serializer = $serializer ?: SerializerBuilder::create()->build();
    }

    public function handleRequest(Request $request)
    {
        if (null === $this->handler) {
            throw new \LogicException('Cannot handle request without a handler!');
        }

        /** @var ReceiveMessageRequest $incomingRequest */
        $incomingRequest = $this->parseRequest($request);

        $this->handler->handle($incomingRequest);
    }

    /**
     * @param Request $request
     * @return ReceiveMessageRequest
     */
    public function parseRequest(Request $request)
    {
        return $this->serializer
            ->fromArray(
                $request->request->all(),
                'ZL\ChatToolSDK\Messaging\Model\ReceiveMessageRequest'
            );
    }
}
